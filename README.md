# README #

### What is this repository for? ###

A seam tracing API on windows.

### How do I get set up? ###

* Summary of set up

  1. After opening this project in Visual Studio, change the solution configuration to `Release`, and solution platforms to `x64`.
  2. Run the solution.
  3. The released DLL and LIB will be found in `x64\Release\SeamTracing.dll` and `x64\Release\SeamTracing.lib` respectively.

* Dependencies

    This repo is self-dependent, where its dependencies can be found in `SeamTracing\tool\win\lib`.


* How to run tests

	**C++**

    ```
    #include <iostream>
    #include <opencv2/highgui/highgui.hpp>
    #include <opencv2/opencv.hpp>
    #include "SeamTracing.h"

    #define INPUT_IMAGE_SIZE 80  // dimesion of raw image
    #define SPIN_IMAGE_SIZE 64  // dimension of input/output image for new model
    //#define SPIN_IMAGE_SIZE 48  // input dimension of old model
    #define DLL_PATH "C:/Users/Rapsodo/Downloads/new_model/deploy.dll"
    #define JSON_PATH "C:/Users/Rapsodo/Downloads/new_model/deploy.json"
    #define PARAMS_PATH "C:/Users/Rapsodo/Downloads/new_model/deploy.params"
    #define IMAGE_PATH "C:/Users/Rapsodo/output_data500/Template_spin1_angle_270.png"

    int main()
    {
      // Read and resized image
      cv::Mat raw_image;
      cv::Mat image;
      raw_image = cv::imread(IMAGE_PATH, CV_8UC1);
      cv::resize(raw_image, image, cv::Size(SPIN_IMAGE_SIZE, SPIN_IMAGE_SIZE));

      // Normalize input image to 0.0 to 1.0
      float *spinImageInput = new float[INPUT_IMAGE_SIZE * INPUT_IMAGE_SIZE];
      for (int i = 0; i < SPIN_IMAGE_SIZE * SPIN_IMAGE_SIZE; ++i) {
        *(spinImageInput->data + i) = *(image.data + i) / 255.0;
      }

      // Make prediction
      float *seamImage = new float[SPIN_IMAGE_SIZE * SPIN_IMAGE_SIZE];
      BaseballSeamTracing seam_tracing;
      seam_tracing.LoadModel(DLL_PATH, JSON_PATH, PARAMS_PATH);
      seam_tracing.Predict(spinImageInput->data, seamImage, SPIN_IMAGE_SIZE);

      //SoftballInitModel(DLL_PATH, JSON_PATH, PARAMS_PATH);
      //SoftballPredict(spinImageInput->data, seamImage, SPIN_IMAGE_SIZE);

      // Resize and show results
      cv::Mat floatSeamMat(SPIN_IMAGE_SIZE, SPIN_IMAGE_SIZE, CV_32FC1, seamImage);
      cv::Mat resizedFloatSeamMat;
      cv::resize(floatSeamMat, resizedFloatSeamMat, cv::Size(INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE));
      cv::Mat seamMat(INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE, CV_8UC1);
      resizedFloatSeamMat.convertTo(seamMat, CV_8UC1, 255.0);
      cv::imshow("seam", seamMat);
      cv::waitKey(0);

      delete[] spinImageInput;
      delete[] seamImage;
    }
    ```

    **C**

    ```
    #include <stdio.h>
    #include <stdlib.h>
    #include "SeamTracing.h"

    #define INPUT_IMAGE_SIZE 80  // dimesion of raw image
    #define SPIN_IMAGE_SIZE 64  // dimension of input/output image for new model
    //#define SPIN_IMAGE_SIZE 48  // input dimension of old model
    #define DLL_PATH "C:/Users/Rapsodo/Downloads/new_model/deploy.dll"
    #define JSON_PATH "C:/Users/Rapsodo/Downloads/new_model/deploy.json"
    #define PARAMS_PATH "C:/Users/Rapsodo/Downloads/new_model/deploy.params"

    int main()
    {
        float input_image[SPIN_IMAGE_SIZE * SPIN_IMAGE_SIZE] = ...;
        float seam_image[SPIN_IMAGE_SIZE * SPIN_IMAGE_SIZE] = ...;

        BaseballInitModel(DLL_PATH, JSON_PATH, PARAMS_PATH);
        BaseballPredict(input_image, seam_image, SPIN_IMAGE_SIZE);

        return 0;
    }
    ```

* Deployment instructions

  1. Copy `x64\Release\SeamTracing.dll` to your root project directory.
  2. Specify the additional include directories, additional library directories, and additional dependencies.


### Who do I talk to? ###

* Should you have any issue, please drop a message to *kentloong.tan@rapsodo.com*.