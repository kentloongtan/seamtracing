#include "pch.h"
#include "SeamTracing.h"
#include <iostream>
#include <iterator>
#include <fstream>
#include <memory>

#if defined(_WINDOWS) 
#include "dlpack/dlpack.h"
#include "tvm/runtime/module.h"
#include "tvm/runtime/registry.h"
#include "tvm/runtime/packed_func.h"
#else
#include "module.h"
#include "registry.h"
#include "packed_func.h"
#endif

// Initialize TVM model from paths
// @param dll_path
// @param json_path
// @param params_path
EXTERN_C void BaseballInitModel(const char* dll_path, const char* json_path, const char* params_path) {
  BaseballSeamTracing::instance()->LoadModel(dll_path, json_path, params_path);
}

// Predict the seam curve on the ball
// @param input_image Floating point image range from 0.0. to 1.0.
// @param output_image Floating point image range from 0.0 to 1.0.
// @param image_width The width of input image, assuming the input is a square image.
EXTERN_C void BaseballPredict(float* input_image, float* output_image, int image_width) {
  BaseballSeamTracing::instance()->Predict(input_image, output_image, image_width);
}

// Initialize TVM model from paths
// @param dll_path
// @param json_path
// @param params_path
EXTERN_C void SoftballInitModel(const char* dll_path, const char* json_path, const char* params_path) {
  SoftballSeamTracing::instance()->LoadModel(dll_path, json_path, params_path);
}

// Predict the seam curve on the ball
// @param input_image Floating point image range from 0.0. to 1.0.
// @param output_image Floating point image range from 0.0 to 1.0.
// @param image_width The width of input image, assuming the input is a square image.
EXTERN_C void SoftballPredict(float* input_image, float* output_image, int image_width) {
  SoftballSeamTracing::instance()->Predict(input_image, output_image, image_width);
}


/// Base class ///

struct SeamTracing::SeamTracingPriv {
  tvm::runtime::Module* tvm_module_;
};

// ctor
SeamTracing::SeamTracing() : mPriv(new SeamTracingPriv) {
}

// dtor
SeamTracing::~SeamTracing() {
  if (mPriv) {
    delete mPriv;
    mPriv = nullptr;
  }
}

// Load TVM model from paths
// @param dll_path
// @param json_path
// @param params_path
void SeamTracing::LoadModel(std::string dll_path, std::string json_path, std::string params_path) {
  if (mPriv) {
    // Load JSON
    std::ifstream json_file(json_path);
    std::string json_data((std::istreambuf_iterator<char>(json_file)), std::istreambuf_iterator<char>());
    json_file.close();

    // Load params
    std::ifstream params_file(params_path, std::ios::binary);
    std::string params_data((std::istreambuf_iterator<char>(params_file)), std::istreambuf_iterator<char>());
    params_file.close();

    // Load DLL
    tvm::runtime::Module mod_syslib = tvm::runtime::Module::LoadFromFile(dll_path, "dll");

    // Get global function module for graph runtime
#if defined(CUDA)
    int device_type = kDLGPU; //cuda
#else
    int device_type = kDLCPU;
#endif
    int device_id = 0;
    tvm::runtime::Module mod = (*tvm::runtime::Registry::Get("tvm.graph_runtime.create"))(json_data, mod_syslib, device_type, device_id);
    mPriv->tvm_module_ = new tvm::runtime::Module(mod);
    tvm::runtime::PackedFunc load_params = mPriv->tvm_module_->GetFunction("load_params");

    TVMByteArray params_bytes;
    params_bytes.data = params_data.c_str();
    params_bytes.size = params_data.length();
    load_params(params_bytes);
  }
}

// Predict the seam curve on the ball
// @param input_image Floating point image range from 0.0. to 1.0.
// @param output_image Floating point image range from 0.0 to 1.0.
// @param image_width The width of input image, assuming the input is a square image.
void SeamTracing::Predict(float* input_image, float* output_image, int image_width) {
  if (mPriv) {
    DLTensor* input;
    const int64_t in_shape[] = { 1, 1, image_width, image_width };
    constexpr int in_ndim = 4;
    constexpr int dtype_code = kDLFloat;
    constexpr int dtype_bits = 32;
    constexpr int dtype_lanes = 1;
#if defined(CUDA)
    constexpr int device_type = kDLGPU;
#else
    constexpr int device_type = kDLCPU;
#endif
    constexpr int device_id = 0;
    int64_t out_shape[] = { 1, 1, image_width, image_width };
    int out_ndim = 4;
    DLTensor* res;

    TVMArrayAlloc(in_shape, in_ndim, dtype_code, dtype_bits, dtype_lanes, device_type, device_id, &input);
    TVMArrayAlloc(out_shape, out_ndim, dtype_code, dtype_bits, dtype_lanes, device_type, device_id, &res);
    TVMArrayCopyFromBytes(input, input_image, (uint64_t)image_width * image_width * 4);

    tvm::runtime::PackedFunc set_input = mPriv->tvm_module_->GetFunction("set_input", false);
    tvm::runtime::PackedFunc run = mPriv->tvm_module_->GetFunction("run", false);
    tvm::runtime::PackedFunc get_output = mPriv->tvm_module_->GetFunction("get_output", false);

    set_input("input_2", input);
    run();
    get_output(0, res);

    memcpy(output_image, res->data, (uint64_t)image_width * image_width * sizeof(float));

    // Free memory
    TVMArrayFree(input);
    TVMArrayFree(res);
  }
}
