#pragma once

// Tell the compiler(or actually the preprocessor) to skip over the definitions
// of `min` and `max` in `windows.h`, to avoid the compile errors while
// including `tvm` library.
// See https://stackoverflow.com/questions/13416418/define-nominmax-using-stdmin-max
#define NOMINMAX

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files
#include <windows.h>
