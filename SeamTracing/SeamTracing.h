#pragma once

#ifdef SEAMTRACING_EXPORTS
#define SEAMTRACING_API __declspec(dllexport)
#else
#define SEAMTRACING_API __declspec(dllimport)
#endif

#if defined(__cplusplus)
#define EXTERN_C extern "C"
#else
#define EXTERN_C
#endif

EXTERN_C SEAMTRACING_API void BaseballInitModel(const char* dll_path, const char* json_path, const char* params_path);
EXTERN_C SEAMTRACING_API void BaseballPredict(float* input_image, float* output_image, int image_width);

EXTERN_C SEAMTRACING_API void SoftballInitModel(const char* dll_path, const char* json_path, const char* params_path);
EXTERN_C SEAMTRACING_API void SoftballPredict(float* input_image, float* output_image, int image_width);


#if defined(__cplusplus)
#include <string>

class SEAMTRACING_API SeamTracing {
private:
  struct SeamTracingPriv;
  SeamTracingPriv* mPriv;

public:
  SeamTracing();
  ~SeamTracing();

  void LoadModel(std::string dll_path, std::string json_path, std::string params_path);
  void Predict(float* input_image, float* output_image, int image_width);
};

class SEAMTRACING_API BaseballSeamTracing : public SeamTracing {
public:
  static SeamTracing* instance() {
    static SeamTracing seam_tracing;
    return &seam_tracing;
  }
};

class SEAMTRACING_API SoftballSeamTracing : public SeamTracing {
public:
  static SeamTracing* instance() {
    static SeamTracing seam_tracing;
    return &seam_tracing;
  }
};

#endif
